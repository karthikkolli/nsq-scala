import sbt._
import sbt.Keys._
import com.typesafe.sbt.SbtSite._
import scoverage.ScoverageSbtPlugin
import sbt.LocalProject
import sbt.Tests.{InProcess, Group}

object Resolvers {
  val typesafe = Seq(
    "Typesafe repository snapshots" at "http://repo.typesafe.com/typesafe/snapshots/",
    "Typesafe repository releases" at "http://repo.typesafe.com/typesafe/releases/"
  )
  val resolversList = typesafe
}

object Dependencies {
  val akkaVersion = "2.3.6"

  import sbt._

  val akkaActor = "com.typesafe.akka" %% "akka-actor" % akkaVersion
  val akkaAgent = "com.typesafe.akka" % "akka-agent_2.11" % akkaVersion
  val akkaTestkit = "com.typesafe.akka" %% "akka-testkit" % akkaVersion
  val snappy = "org.xerial.snappy" % "snappy-java" % "1.1.1.3"
  val playJson = "com.typesafe.play" %% "play-json" % "2.4.0-M1"
  val sprayHttp = "io.spray" %% "spray-http" % "1.3.2"
  val sprayCan = "io.spray" %% "spray-can" % "1.3.2"
  val sprayHttpx = "io.spray" %% "spray-httpx" % "1.3.2"
  val sprayClient = "io.spray" %% "spray-client" % "1.3.2"
  val sprayTestKit = "io.spray" %% "spray-testkit" % "1.3.2" % "test"
  val jsnappy = "com.avast" % "bytecompressor-jsnappy_2.11" % "1.2.2"
  val scalaTest = "org.scalatest" %% "scalatest" % "2.2.0"

  val nsqDependencies = Seq(
    akkaActor,
    akkaAgent,
    playJson,
    snappy,
    jsnappy,
    sprayHttp,
    sprayCan,
    sprayHttpx,
    sprayClient,
    akkaTestkit % "test",
    scalaTest
  )
}

object MainBuild extends Build {
  val baseSourceUrl = "https://tobefilled/tree/"

  val v = "0.1.0"

  lazy val standardSettings = Defaults.defaultSettings ++
    Seq(
      name := "nsq-scala",
      version := v,
      organization := "com.nsq",
      scalaVersion := "2.11.3",
      crossScalaVersions := Seq("2.11.3", "2.10.4"),
      licenses += ("Apache-2.0", url("http://www.apache.org/licenses/LICENSE-2.0.html")),
      resolvers ++= Resolvers.resolversList,

      publishMavenStyle := true,

      scalacOptions in (Compile, doc) <++= baseDirectory in LocalProject("nsq-scala") map { bd =>
        Seq(
          "-sourcepath", bd.getAbsolutePath
        )
      },
      scalacOptions in (Compile, doc) <++= version in LocalProject("nsq-scala") map { version =>
        val branch = if(version.trim.endsWith("SNAPSHOT")) "master" else version
        Seq[String](
          "-doc-source-url", baseSourceUrl + branch +"€{FILE_PATH}.scala",
          "-doc-title", "NSQScala "+v+" API",
          "-doc-version", version
        )
      }
  ) ++ site.settings ++ site.includeScaladoc(v +"/api") ++ site.includeScaladoc("latest/api")

  lazy val BenchTest = config("bench") extend Test

  lazy val benchTestSettings = inConfig(BenchTest)(Defaults.testSettings ++ Seq(
    sourceDirectory in BenchTest <<= baseDirectory / "src/benchmark",
    //testOptions in BenchTest += Tests.Argument("-preJDK7"),
    testFrameworks in BenchTest := Seq(new TestFramework("org.scalameter.ScalaMeterFramework")),

    //https://github.com/sbt/sbt/issues/539 => bug fixed in sbt 0.13.x
    testGrouping in BenchTest <<= definedTests in BenchTest map partitionTests
  ))

  lazy val root = Project(id = "nsq-scala",
    base = file("."),
    settings = standardSettings ++ Seq(
      libraryDependencies ++= Dependencies.nsqDependencies
    )
      ++ ScoverageSbtPlugin.instrumentSettings
      ++ CoverallsPlugin.coverallsSettings
  ).configs(BenchTest)
    //.settings(benchTestSettings: _* )

  def partitionTests(tests: Seq[TestDefinition]) = {
    Seq(new Group("inProcess", tests, InProcess))
  }
}
