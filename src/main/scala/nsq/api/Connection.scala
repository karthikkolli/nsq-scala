package nsq.api.connection

import akka.util.ByteString
import nsq.ByteStringSerializer._
import nsq._
import nsq.config.NSQConfig

import scala.concurrent.duration.{Duration, DurationInt}

case object MagicV2 extends NSQWriteCommand {
    //New line is not required
  val encodedRequest: ByteString = encodeBasic("  V2")
}

case class IdentifyConfigData(config: NSQConfig) extends NSQWriteCommand {
  import nsq.serializers.NSQConfigFormats._
    val msg = configFormats.writes(config)
  val body = String.serialize(msg.toString())
  val encodedRequest: ByteString = encode("IDENTIFY", Seq.empty, body)
}

case class Auth(secret: String) extends  NSQWriteCommand {
    val body = String.serialize(secret)
  val encodedRequest: ByteString = encode("REGISTER", Seq.empty, body)
}

case class Subscribe(topic: String, channel: String) extends NSQWriteCommand {
    val params = Seq[ByteString](
    String.serialize(topic),
    String.serialize(channel)
  )
  val encodedRequest: ByteString = encode("SUB", params)
}

case class Ready(count: Long) extends NSQWriteCommand {
    val params = Seq[ByteString](LongConverter.serialize(count))
  val encodedRequest: ByteString = encode("RDY", params)
}

case class Finish(id : ByteString) extends NSQWriteCommand {
    val params = Seq[ByteString](id)
  val encodedRequest: ByteString = encode("FIN", params)
}

case class ReQueue(id: ByteString, duration: Duration = 0.seconds) extends NSQWriteCommand {
    val params = Seq[ByteString](
    id,
    LongConverter.serialize(duration.toSeconds)
  )
  val encodedRequest: ByteString = encode("REQ", params)
}

case class Touch(id: ByteString) extends NSQWriteCommand {
    val params = Seq[ByteString](id)
  val encodedRequest: ByteString = encode("TOUCH", params)
}

case object StartClose extends NSQWriteCommand {
    val encodedRequest: ByteString = encode("CLS")
}

case object NOP extends NSQWriteCommand {
    val encodedRequest: ByteString = encode("NOP")
}