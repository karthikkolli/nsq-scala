package nsq.api

import akka.util.ByteString
import nsq.NSQWriteCommand

//case class Publish[A](topic: String, body: A)(implicit convert: ByteStringSerializer[A]) extends NSQWriteCommandStatusString {
//  val isMasterOnly = true
//  val encodedRequest: ByteString = encode("PUB", Seq(ByteString(topic)), convert.serialize(body))
//}
//
//case class MultiPublish[A](topic: String, messages: Seq[A])(implicit convert: ByteStringSerializer[A]) extends NSQWriteCommandStatusString {
//  val isMasterOnly = true
//  val msgBytes:Seq[ByteString] = MultiMessageSerializer.toSeqByteString[A](messages)
//  val encodedRequest: ByteString = encode("MULTIPUB", Seq(ByteString(topic)), msgBytes)
//}

case class Publish(topic: String, body: ByteString) extends NSQWriteCommand {
  val isMasterOnly = true
  val encodedRequest: ByteString = encode("PUB", Seq(ByteString(topic)), body)
}

case class MultiPublish (topic: String, messages: Seq[ByteString]) extends NSQWriteCommand {
  val isMasterOnly = true
  val encodedRequest: ByteString = encode("MPUB", Seq(ByteString(topic)), messages)
}