package nsq.protocol

import java.nio.charset.Charset

import akka.util.ByteString
import nsq.ByteStringSerializer._

import scala.collection.mutable.ArrayBuffer

object NSQProtocolRequest {
  val UTF8_CHARSET = Charset.forName("UTF-8")
  val SPACE = " "
  val BYTE_SPACE = SPACE.getBytes(UTF8_CHARSET)
  val NEW_LINE = "\n"
  val BYTE_NEW_LINE = NEW_LINE.getBytes(UTF8_CHARSET)

  def multiBulk(command: String, test: Seq[ByteString]): ByteString = {
    //Old method
    ByteString.empty
  }

  def multiBulk(command: String, args: Seq[ByteString], bodies: Seq[ByteString]): ByteString = {
    val data = new ArrayBuffer[Byte]()
    data ++= command.getBytes(UTF8_CHARSET) ++
      args.foldLeft(new ArrayBuffer[Byte]())(
        (b, a) => b ++= (BYTE_SPACE ++ a)
      ) ++ BYTE_NEW_LINE

      bodies.length match {
        case 0 => //
        case 1 =>
          val msg = bodies.head
          data ++= (integer2Byte(msg.length) ++ msg)
        case _ =>
          val multiData = new ArrayBuffer[Byte]()
          multiData ++= integer2Byte(bodies.length)
          bodies.foldLeft(multiData)(
            (b, a) => b ++= (integer2Byte(a.length) ++ a)
          )
          data ++= (integer2Byte(multiData.toArray.length) ++ multiData)
      }

    val bs = ByteString(data.toArray)
    bs
  }

  def inline(command: String): ByteString = ByteString(command)
}
