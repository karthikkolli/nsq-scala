package nsq.config

import java.net.{InetAddress, InetSocketAddress}
import java.util.UUID

import nsq.strategies.{ReconnectStrategy, RetryStrategy}

import scala.concurrent.duration.{Duration, DurationInt, FiniteDuration}

case class TlsConfig(certs: String)

case class ClientInfo(clientId: String, hostname: String)

object DefaultClientInfo extends ClientInfo(UUID.randomUUID().toString,InetAddress.getLocalHost.getHostName)

case class NSQConfig(clientInfo: ClientInfo = DefaultClientInfo,
                     heartbeatInterval: Duration = 30.seconds,
                     tlsOptions: Option[TlsConfig] = None,
                     compressionType: CompressionType.CompressionType = CompressionType.NONE,
                     compressionLevel: CompressionLevel.CompressionLevel = CompressionLevel.NONE,
                     authSecret: String = "hello",
                     outputBufferSize: Int = 16384,
                     outputBufferTimeout: FiniteDuration = 250.milliseconds,
                     maxInFlight: Long = 5,
                     maxBackoffDuration: FiniteDuration = 90.seconds,
                     maxConnections : Int = 4,
                     maxConnectionsPerAddress : Int = 2,
                     sampleRate: Int = 0,
                     messageTimeout: FiniteDuration = 2.seconds,
                     nsqdTCPAddresses : Seq[InetSocketAddress] = Seq.empty[InetSocketAddress],
                     lookupHttpAddresses :Seq[InetSocketAddress] = Seq.empty[InetSocketAddress],
                     lookupdPollInterval : FiniteDuration = 60.seconds,
                     lookupdPollJitter: Double = 0.3,
                     retryStrategy: RetryStrategy.RetryStrategy = RetryStrategy.NONE,
                     reconnectStrategy: ReconnectStrategy.ReconnectStrategy = ReconnectStrategy.NONE,
                     minReconnectDuration : FiniteDuration = 1.second,
                     maxReconnectDuration : FiniteDuration = 60.seconds) {

  val userAgent = "nsq-scala " + nsq.VERSION

  def isValid: Boolean = {
    (heartbeatInterval gteq 1.second) &&
      (heartbeatInterval lteq 1.minute) &&
      (outputBufferSize >= 64) &&
      (outputBufferTimeout >= 1.millisecond) &&
      (sampleRate >= 0 && sampleRate <= 99) &&
      (lookupdPollJitter > 0 && lookupdPollJitter < 1)
  }
  def isValidConsumer: Boolean = {
    isValid &&
      ((nsqdTCPAddresses.length > 0 && lookupHttpAddresses.length == 0)
        || (nsqdTCPAddresses.length == 0 && lookupHttpAddresses.length > 0))
  }

  def isValidProducer: Boolean = isValid
}

object DefaultNSQConfig{
  def apply(nsqdAddresses: Seq[InetSocketAddress]) = NSQConfig(
    heartbeatInterval = 60.seconds,
    nsqdTCPAddresses = nsqdAddresses)
}

object DefaultSnappyConfig{
  def apply(nsqdAddresses: Seq[InetSocketAddress]) = NSQConfig(
    heartbeatInterval = 10.seconds,
    compressionType = CompressionType.SNAPPY,
    nsqdTCPAddresses = nsqdAddresses)
}

object DefaultDeflateConfig{
  def apply(nsqdAddresses: Seq[InetSocketAddress]) = NSQConfig(
    heartbeatInterval = 10.seconds,
    compressionType = CompressionType.DEFLATE,
    compressionLevel = CompressionLevel(6),
    nsqdTCPAddresses = nsqdAddresses)
}

