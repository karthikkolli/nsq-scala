package nsq

package object config {

  object CompressionType extends Enumeration {
    type CompressionType = Value
    val NONE = Value(0)
    val SNAPPY = Value(1)
    val DEFLATE = Value(2)
  }

  object CompressionLevel extends Enumeration {
    type CompressionLevel = Value
    val NONE = Value(0)
    val ONE = Value(1)
    val TWO = Value(2)
    val THREE = Value(3)
    val FOUR = Value(4)
    val FIVE = Value(5)
    val SIX = Value(6)
    val SEVEN = Value(7)
    val EIGHT = Value(8)
    val NINE = Value(9)

  }
}

