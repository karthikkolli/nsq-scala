package nsq

import akka.util.ByteString
import nsq.protocol._


trait NSQWriteCommand {

  val encodedRequest: ByteString

  def encodeBasic(command:String) = ByteString(command)

  def encode(command: String) = NSQProtocolRequest.multiBulk(command, Seq.empty, Seq.empty)

  def encode(command: String, args: Seq[ByteString]) = NSQProtocolRequest.multiBulk(command, args, Seq.empty[ByteString])

  def encode(command: String, args: Seq[ByteString], body: ByteString) = NSQProtocolRequest.multiBulk(command, args, Seq(body))

  def encode(command:String, args: Seq[ByteString], bodies: Seq[ByteString]) = NSQProtocolRequest.multiBulk(command, args, bodies)
}

trait NSQResponse