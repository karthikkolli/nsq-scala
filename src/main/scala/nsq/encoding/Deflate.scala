package nsq.encoding

import java.util.zip.{Inflater, DataFormatException, ZipException, Deflater}

import akka.util.{ByteStringBuilder, ByteString}

import scala.annotation.tailrec

class DeflateCompressor(compression: Int) extends Compressor {
  protected lazy val deflater = new Deflater(compression)

  override final def compressAndFlush(input: ByteString): ByteString = {
    val buffer = newTempBuffer(input.size)

    compressWithBuffer(input, buffer) ++ flushWithBuffer(buffer)
  }

  override final def compressAndFinish(input: ByteString): ByteString = {
    val buffer = newTempBuffer(input.size)

    compressWithBuffer(input, buffer) ++ finishWithBuffer(buffer)
  }

  override final def compress(input: ByteString): ByteString = compressWithBuffer(input, newTempBuffer())

  override final def flush(): ByteString = flushWithBuffer(newTempBuffer())

  override final def finish(): ByteString = finishWithBuffer(newTempBuffer())

  protected def compressWithBuffer(input: ByteString, buffer: Array[Byte]): ByteString = {
    assert(deflater.needsInput())
    deflater.setInput(input.toArray)
    drain(buffer)
  }

  protected def flushWithBuffer(buffer: Array[Byte]): ByteString = {
    // trick the deflater into flushing: switch compression level
    // FIXME: use proper APIs and SYNC_FLUSH when Java 6 support is dropped
    deflater.deflate(Array.emptyByteArray, 0, 0)
    deflater.setLevel(-1)
    val res1 = drain(buffer)
    deflater.setLevel(compression)
    val res2 = drain(buffer)
    res1 ++ res2
  }

  protected def finishWithBuffer(buffer: Array[Byte]): ByteString = {
    deflater.finish()
    val res = drain(buffer)
    deflater.end()
    res
  }

  @tailrec
  protected final def drain(buffer: Array[Byte], result: ByteStringBuilder = new ByteStringBuilder()): ByteString = {
    val len = deflater.deflate(buffer)
    if (len > 0) {
      result ++= ByteString.fromArray(buffer, 0, len)
      drain(buffer, result)
    } else {
      assert(deflater.needsInput())
      result.result()
    }
  }

  private def newTempBuffer(size: Int = 65536): Array[Byte] =
  // The default size is somewhat arbitrary, we'd like to guess a better value but Deflater/zlib
  // is buffering in an unpredictable manner.
  // `compress` will only return any data if the buffered compressed data has some size in
  // the region of 10000-50000 bytes.
  // `flush` and `finish` will return any size depending on the previous input.
  // This value will hopefully provide a good compromise between memory churn and
  // excessive fragmentation of ByteStrings.
    new Array[Byte](size)
}

class DeflateDecompressor extends Decompressor {
  protected lazy val inflater = new Inflater(true)

  def decompress(input: ByteString): ByteString =
    try {
      inflater.setInput(input.toArray)
      drain(new Array[Byte](input.length * 2))
    } catch {
      case e: DataFormatException ⇒
        throw new ZipException(e.getMessage)//.toOption getOrElse "Invalid ZLIB data format")
    }

  @tailrec protected final def drain(buffer: Array[Byte], result: ByteString = ByteString.empty): ByteString = {
    val len = inflater.inflate(buffer)
    if (len > 0) drain(buffer, result ++ ByteString.fromArray(buffer, 0, len))
    else if (inflater.needsDictionary) throw new ZipException("ZLIB dictionary missing")
    else result
  }

  def finish(): ByteString = {
    inflater.end()
    ByteString.empty
  }
}