package nsq.encoding

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}

import akka.util.ByteString
import org.xerial.snappy.{Snappy, SnappyFramedInputStream, SnappyFramedOutputStream}

class SnappyCompressor extends Compressor {

  override final def compressAndFlush(input: ByteString): ByteString = compress(input) ++ flush()

  override final def compressAndFinish(input: ByteString): ByteString = compress(input) ++ finish()

  override final def compress(input: ByteString): ByteString = {

    val os = new ByteArrayOutputStream()
    val ss = new SnappyFramedOutputStream(os)
    ss.write(input.toArray)
    ss.close()
    ByteString(os.toByteArray)
  }

  override final def flush(): ByteString = ByteString.empty

  override final def finish(): ByteString = ByteString.empty

}
object SnappyDecompressor {
  val HEADER = ByteString(0xff, 0x06, 0x00, 0x00, 0x73, 0x4e, 0x61,
    0x50, 0x70, 0x59)
}

class SnappyDecompressor extends Decompressor {
  def decompress(input: ByteString): ByteString = {
    val contents = (SnappyDecompressor.HEADER ++  input).toArray
    val is = new SnappyFramedInputStream(new ByteArrayInputStream(contents))
    val os = new ByteArrayOutputStream(Snappy.uncompressedLength(contents))
    is.transferTo(os)
    os.close()
    ByteString(os.toByteArray)
  }

  def finish(): ByteString = {
    ByteString.empty
  }
}