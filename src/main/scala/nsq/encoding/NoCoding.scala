package nsq.encoding

import akka.util.ByteString

object NoCodingCompressor extends Compressor {
  def compress(input: ByteString): ByteString = input
  def flush() = ByteString.empty
  def finish() = ByteString.empty

  def compressAndFlush(input: ByteString): ByteString = input
  def compressAndFinish(input: ByteString): ByteString = input
}
object NoCodingDecompressor extends Decompressor {
  def decompress(input: ByteString): ByteString = input
  def finish(): ByteString = ByteString.empty
}