package nsq.encoding

import akka.util.ByteString

/** A stateful object representing ongoing decompression. */
abstract class Decompressor {
  /** Decompress the buffer and return decompressed data. */
  def decompress(input: ByteString): ByteString

  /** Flushes potential remaining data from any internal buffers and may report on truncation errors */
  def finish(): ByteString

  /** Combines decompress and finish */
  def decompressAndFinish(input: ByteString): ByteString = decompress(input) ++ finish()
}