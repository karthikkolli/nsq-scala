package nsq.actors

import akka.actor.FSM.{SubscribeTransitionCallBack, UnsubscribeTransitionCallBack}
import akka.actor.{Actor, ActorRef, FSM, Terminated}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import nsq.config.NSQConfig
import nsq.events._
import nsq.events.connection.NSQConnect
import nsq.events.reader._
import nsq.messages.NSQMessage
import scala.concurrent.duration.DurationDouble

import scala.collection.mutable


case class ReaderData(inFlight : Long = 0, availableRdy: Long = 0, connections: Vector[ActorRef] = Vector[ActorRef]()) {
  lazy val router: Router = Router(RoundRobinRoutingLogic(), connections.map(ActorRefRoutee))
}

class ReaderRdy(receiver: ActorRef, config: NSQConfig)
  extends Actor with FSM[ReaderState, ReaderData] {

  var maxInFlight = 0
  var maxBackoffDuration = 0
  var lowRdyTimeout = 1.5.seconds

  startWith(READER_ZERO, ReaderData())


  val connections: mutable.Set[ActorRef] = mutable.Set[ActorRef]()
  var roundRobinConnections : Router = Router(RoundRobinRoutingLogic())

  when(READER_ZERO) {
    case Event(Backoff, d) => stay()
    case Event(Success, d) => stay()
    case Event(TryRead, d) => stay()
    case Event(Pause, d) => goto(READER_PAUSE)
    case Event(UnPause, d) => stay()
  }

  when(READER_PAUSE) {
    case Event(UnPause, d) => goto(READER_TRYONE)
  }

  when(READER_TRYONE) {
    case Event(Backoff, d) => goto(READER_BACKOFF)
    case Event(Success, d) => goto(READER_MAX)
    case Event(Pause, d) => goto(READER_PAUSE)
  }

  when(READER_MAX) {
    case Event(Backoff, d) => goto(READER_BACKOFF)
    case Event(Pause, d) => goto(READER_PAUSE)
  }

  when(READER_BACKOFF) {
    case Event(TryRead, d) => goto(READER_TRYONE)
    case Event(Pause, d) => goto(READER_PAUSE)
  }

  whenUnhandled {
    case Event(Backoff, d) => stay()
    case Event(Success, d) => stay()
    case Event(TryRead, d) => stay()
    case Event(Pause, d) => stay()
    case Event(UnPause, d) => stay()
    case Event(NewConnection(conn), d) =>
      addConnection(conn)
      conn ! NSQConnect
      stay()
    case Event(ConnectionReady, d) =>
      onConnectionReady(sender())
      stay()
    case Event(ConnectionClosed, d) =>
      removeConnection(sender())
      if(connections.size == 0) goto(READER_ZERO)
      else {
        balance()
        stay()
      }
    case Event(MessageReceived(m), d) =>
      onMessageReceived(m,sender())
      stay() using d.copy(inFlight = d.inFlight + 1, availableRdy = d.availableRdy - 1)
    case Event(MessageFinished, d) =>
      onMessageFinished(sender())
      stay() using d.copy(inFlight = d.inFlight - 1)
    case Event(MessageRequeued, d) =>
      onMessageRequeued(sender())
      stay() using d.copy(inFlight = d.inFlight - 1)
    case Event(Terminated(a), d) =>
      removeConnection(a)
      if(connections.size == 0) goto(READER_ZERO)
      else {
        balance()
        stay()
      }
  }

  onTransition {
    case _ -> READER_BACKOFF => backoff()
    case _ -> READER_MAX => bump()
    case _ -> READER_TRYONE => tryMessage()
    case _ -> READER_PAUSE => backoff()
    case _ -> READER_ZERO => clearBackoff()
  }


  def clearBackoff() : Unit = {}

  def backoff() : Unit = {
    stateData.router.route(connection.Backoff, self)
  }

  def tryMessage() : Unit = balance()

  def balance() : Unit = {
    val max = stateName match {
      case READER_TRYONE => 1
      case _ => config.maxInFlight
    }

    val maxInFlightPerConn = if(connections.size > 0)(max/connections.size).toInt else 0

    if(maxInFlightPerConn == 0) {
      /* Backoff on all connections. In-flight messages from connections
            will still be processed */
      backoff()
      //Distribute available RDY count to the connections next in line.
      for(x <- 0L until (config.maxInFlight - stateData.inFlight)){
        stateData.router.route(connection.SetMaxRdy(1), self)
        stateData.router.route(connection.Bump, self)
      }
    } else {
      var remainder = config.maxInFlight % connections.size
      connections.foreach(conn => {
        if(remainder > 0){
          remainder -= 1
          conn ! connection.SetMaxRdy(maxInFlightPerConn + 1)
          conn ! connection.Bump
        } else {
          conn ! connection.SetMaxRdy(maxInFlightPerConn)
          conn ! connection.Bump
        }
      })
    }
  }

  def bump() : Unit = {
    stateData.router.route(connection.Bump, self)
  }

  def isLowRdy : Boolean = config.maxInFlight < connections.size

  def addConnection(conn:ActorRef) = {
    context watch conn
  }

  def removeConnection(conn:ActorRef) = {
    context unwatch conn
    connections -= conn
    roundRobinConnections = roundRobinConnections.removeRoutee(conn)
  }

  def onConnectionReady(conn: ActorRef) = {
    connections += conn
    roundRobinConnections = roundRobinConnections.addRoutee(conn)
    balance()
    stateName match {
      case READER_ZERO => goto(READER_MAX)
      case READER_TRYONE | READER_MAX =>  conn ! connection.Bump
      case _ => //
    }
  }

  def onMessageReceived(message: NSQMessage, actor: ActorRef) = {
    receiver ! message
  }
  def onMessageRequeued(actor: ActorRef) = {
    stateName match {
      case READER_BACKOFF => //
      case _ => actor ! connection.Bump
    }
  }
  def onMessageFinished(actor: ActorRef) = {
    if(isLowRdy) balance()
    else actor ! connection.Bump
  }
}
