package nsq.actors

import java.net.InetSocketAddress
import java.util.UUID
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong

import akka.actor._
import akka.routing.{Broadcast, Router, SmallestMailboxRoutingLogic}
import akka.util.{ByteString, Helpers}
import nsq.NSQWriteCommand
import nsq.api.{MultiPublish, Publish}
import nsq.config.NSQConfig
import nsq.events._
import nsq.events.connection.NSQClose
import nsq.events.reader.ConnectionFailed
import nsq.exceptions.{InvalidConfiguration, NoConnectionException}
import nsq.messages.ConsumerMessages.ConnectUsingTCP
import nsq.strategies.ReconnectStrategy
import nsq.strategies.retry.{NoRetryScheduler, ExponentialRetry}
import nsq.utils.{ConnectionUtils, ProducerOperation}

import scala.collection.mutable
import scala.concurrent.Promise
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.DurationInt

sealed trait ProducerCommand

object StartProducer extends ProducerCommand

object StopProducer extends ProducerCommand

object ReconnectProducer extends ProducerCommand

object ForceStopProducer extends ProducerCommand

object GetProducerStatus extends ProducerCommand

case class PublishMessage(content: ByteString) extends ProducerCommand

case class PublishMultiple(content: Seq[ByteString]) extends ProducerCommand

sealed trait ProducerStatus

object ProducerAvailable extends ProducerStatus

object ProducerUnAvailable extends ProducerStatus

object ProducerStopped extends ProducerStatus

sealed trait ProducerActorCommand {
  val id: UUID
  val content: NSQWriteCommand
}

case class ProducerMessage(id: UUID, content: Publish, retries: Int = 0) extends ProducerActorCommand

case class ProducerMultiMessage(id: UUID, content: MultiPublish, retries: Int = 0) extends ProducerActorCommand


class NSQProducer(address: InetSocketAddress, topic: String, config: NSQConfig) extends Actor with ActorLogging {
  val name = "producerMaster"

  import context._

  override def preStart(): Unit = {
    if (!config.isValidProducer) {
      throw new InvalidConfiguration("Invalid configuration sent")
    }
    if (!ConnectionUtils.isValidTopicName(topic)) {
      throw new InvalidConfiguration("Invalid topic name")
    }
  }

  var router = {
    Router(SmallestMailboxRoutingLogic())
  }

  var isStarted = false
  var isAvailable = false
  var isConnecting = false
  var stopStatus = 0
  var source: ActorRef = null

  var connectionScheduler = config.reconnectStrategy match {
    case ReconnectStrategy.EXPONENTIAL_RECONNECT => new ExponentialRetry(minInterval = config.minReconnectDuration ,
      maxInterval = config.maxReconnectDuration)
    case ReconnectStrategy.NONE => NoRetryScheduler
  }
  def scheduleReconnect() : Unit =  {
    connectionScheduler.failed()
    if(connectionScheduler.hasNext) {
      val reconnectDuration = connectionScheduler.nextReconnect()
      log.info(s"Trying to reconnect in $reconnectDuration")
      this.context.system.scheduler.scheduleOnce(reconnectDuration, self, ReconnectProducer)
    }else {
      log.info("Automatic reconnect disabled. Reconnect manually")
    }
  }


  //Public API
  def receive: Receive = {
    case StartProducer =>
      if (!isStarted) {
        connectUsingTCP()
        source = sender()
      }
    case GetProducerStatus =>
      if (isStarted && isAvailable) sender() ! ProducerAvailable
      else if (stopStatus > 0) sender() ! ProducerStopped
      else sender() ! ProducerUnAvailable

    case StopProducer =>
      stopStatus = 1
      isAvailable = false
      router.route(Broadcast(NSQClose), self)

    case ForceStopProducer =>
      stopStatus = 2
      isAvailable = false
      router.route(Broadcast(PoisonPill), self)

    case w: PublishMessage =>
      val promise = Promise[ProducerResponse]()
      if (!isAvailable) {
        sender() ! promise.failure(NoConnectionException)
      } else {
        val pubMsg = Publish(topic, w.content)
        val msgId = UUID.randomUUID()
        val msg = ProducerMessage(msgId, pubMsg)

        router.route(ProducerOperation(msg, promise), sender())
        sender() ! promise.future
      }
    case w: PublishMultiple =>
      val promise = Promise[ProducerResponse]()
      val msgSize = w.content.foldLeft(0L)((sum, item) => sum + item.length)
      if (msgSize > 2500000) {
        sender() ! promise.failure(MessageTooBigException(msgSize))
      }
      else if (!isAvailable) {
        sender() ! promise.failure(NoConnectionException)
      } else {
        val pubMsg = MultiPublish(topic, w.content)
        val msgId = UUID.randomUUID()
        val msg = ProducerMultiMessage(msgId, pubMsg)

        router.route(ProducerOperation(msg, promise), sender())
        sender() ! promise.future
      }

    case reader.ConnectionReady =>
      context watch sender()
      router = router.addRoutee(sender())
      isStarted = true
      isAvailable = true
      isConnecting = false
      connectionScheduler.reset()
      log.info("Connection successful")

    case ReconnectProducer =>
      if(!isAvailable){
        connectUsingTCP()
      }
    case ConnectionFailed =>
      //Can also fetch next available address and send via address changed
      isAvailable = false
      isConnecting = false
      sender() ! PoisonPill
      scheduleReconnect()

    case Terminated(a) =>
      if(isAvailable) {
        isAvailable = false
        scheduleReconnect()
      }
  }

  def connectToNSQD(addr: InetSocketAddress): Unit = {
    if(isConnecting || isAvailable) return
    isConnecting = true
    val conn = context.actorOf(
      Props(classOf[NSQProducerConnection], address, config, topic, self)
        .withDispatcher(NSQProdDispatcher.dispatcher),
      name + '-' + NSQProdDispatcher.tempName()
    )
    conn ! connection.NSQConnect
    log.info("Connecting to NSQ Client {}", addr)
  }

  def connectUsingTCP(): Unit = {
    connectToNSQD(address)
  }

}

private[nsq] object NSQProdDispatcher {

  val dispatcher = "nsqscala.producer-dispatcher"

  val tempNumber = new AtomicLong

  def tempName() = Helpers.base64(tempNumber.getAndIncrement)

}