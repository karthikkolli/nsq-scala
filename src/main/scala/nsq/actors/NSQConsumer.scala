package nsq.actors

import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong

import akka.actor._
import akka.dispatch.{PriorityGenerator, UnboundedPriorityMailbox}
import akka.pattern.ask
import akka.util.{Timeout, Helpers}
import com.typesafe.config.Config
import nsq.config.NSQConfig
import nsq.events._
import nsq.events.reader._
import nsq.exceptions.{NSQException, InvalidConfiguration}
import nsq.messages.ConsumerMessages.{StopConsumer, ConnectUsingLookupd, ConnectUsingTCP, StartConsumer}
import nsq.messages.LookupMessages.{NSQClientAddress, Lookup}
import nsq.messages.NSQMessage
import nsq.utils.{ConnectionUtils, Operation}

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.duration.{FiniteDuration, DurationInt}
import scala.util.{Failure, Success}

class ConsumerMailbox(settings: ActorSystem.Settings, config: Config)
  extends UnboundedPriorityMailbox(
    PriorityGenerator {
      //    case x:RDYEvents => 0
      case x: Operation[_] => 1
      case PoisonPill => 3
      case otherwise => 2
    })


/**
 * Responsibilities
 * 1. Responsible for periodically querying the nsqlookupds
 * 2. Connects and subscribes to discovered/configured nsqd
 * 3. Consumes messages and triggers MESSAGE events
 * 4. Starts the ReaderRdy instance
 * 5. Hands nsqd connections to the ReaderRdy instance
 **/
class NSQConsumer(topic: String, channel: String, config: NSQConfig, receiver: ActorRef)
  extends Actor
  with ActorLogging {

  override def preStart(): Unit = {
    if (!config.isValidConsumer ){
      throw new InvalidConfiguration("Invalid configuration sent")
    }
    if(!ConnectionUtils.isValidTopicName(topic)){
      throw new InvalidConfiguration("Invalid topic name")
    }
    if (!ConnectionUtils.isValidChannelName(channel)) {
      throw new InvalidConfiguration("Invalid channel name")
    }
  }

  import context.dispatcher

  val name = "consumer"
  val roundRobinLookupd = Iterator.continually(config.lookupHttpAddresses).flatten
  val addresses = config.nsqdTCPAddresses
  val readerRdy = context.actorOf(
    Props(classOf[ReaderRdy], receiver, config)
      .withDispatcher(NSQConsumerDispatcher.dispatcher),
    name + "-reader-rdy"
  )

  val connectionMap = new mutable.HashMap[InetSocketAddress, Int]()
  val actorAddrMap = new mutable.HashMap[ActorRef, InetSocketAddress]()
  var paused: Boolean = false
  var connectionScheduler : Cancellable = null
  var numConnections : Int = 0
  implicit val lookupTimeout: Timeout = 30.seconds

  val nsqLookup = context.actorOf(
    Props(classOf[NSQLookup])
      .withDispatcher(NSQConsumerDispatcher.dispatcher),
    name + '-' + NSQConsumerDispatcher.tempName()
  )
  //TODO: Add a timer for continuous lookup of servers

  def receive: Receive = {
    case StartConsumer =>
      val interval = config.lookupdPollInterval
      val delay: FiniteDuration = FiniteDuration((interval.toMillis * config.lookupdPollJitter).toLong,
        TimeUnit.MILLISECONDS)
      if (!isPaused) {
        if (addresses.length > 0) {
          connectUsingTCP()
          connectionScheduler =
            context.system.scheduler.schedule(0 milliseconds,
              delay,
              self,
              ConnectUsingTCP)
        } else {
          queryLookupd()
          connectionScheduler =
            context.system.scheduler.schedule(0 milliseconds,
              delay,
              self,
              ConnectUsingLookupd)
        }
      }
    case ConnectUsingTCP => connectUsingTCP()
    case ConnectUsingLookupd => queryLookupd()
    case ConnectionFailed =>
      //Can also fetch next available address and send via address changed
      sender() ! PoisonPill
      removeConnection(sender())

    case m:NSQMessage => receiver ! m
    case StopConsumer =>
  }

  def isPaused: Boolean = paused

  def connectToNSQD(addr: InetSocketAddress): Unit = {
    if (numConnections >= config.maxConnections) return
    val connectionsForAddress = connectionMap.getOrElse(addr, 0)
    if (connectionsForAddress >= config.maxConnectionsPerAddress) return

    val conn = context.actorOf(
      Props(classOf[NSQConsumerConnection], addr, config, topic, channel, readerRdy)
        .withDispatcher(NSQConsumerDispatcher.dispatcher),
      name + '-' + NSQConsumerDispatcher.tempName()
    )
    log.info("Connecting to NSQ Client {}", addr)
    readerRdy ! reader.NewConnection(conn)
    actorAddrMap += (conn -> addr)
    connectionMap += (addr -> (connectionsForAddress + 1))
    numConnections += 1
  }

  def connectUsingTCP(): Unit = {
    log.info("Looking up for new servers")
    addresses.foreach(connectToNSQD)
  }

  def queryLookupd(): Unit = {
    val addr = roundRobinLookupd.next()
    val f1 = (nsqLookup ? Lookup(Seq(addr), topic)).mapTo[Future[Seq[NSQClientAddress]]]
    f1 flatMap identity onComplete {
      case Success(clients) if clients.size > 0 =>
        clients.foreach(addr => connectToNSQD(new InetSocketAddress(addr.addr, addr.port)))
      case _ => //
    }
  }

  def removeConnection(conn: ActorRef) = {
    val tuple = for {
      addr <- actorAddrMap.get(conn)
      value <- connectionMap.get(addr)
    } yield (addr, value)
    tuple.foreach(t => {
      connectionMap += (t._1 -> (t._2 - 1))
      actorAddrMap.remove(conn)
      log.info("Connection closed {}", t._1)
      numConnections -= 1
    })

  }
}

private[nsq] object NSQConsumerDispatcher {

  val dispatcher = "nsqscala.consumer-dispatcher"

  val tempNumber = new AtomicLong

  def tempName() = Helpers.base64(tempNumber.getAndIncrement)

}