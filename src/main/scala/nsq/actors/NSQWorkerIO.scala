package nsq.actors

import java.net.InetSocketAddress

import akka.actor._
import akka.io.Tcp
import akka.io.Tcp.{CommandFailed, Connect, Connected, Received, Register, _}
import akka.util.ByteString
import nsq.NSQWriteCommand
import nsq.api.connection.{Auth, IdentifyConfigData, MagicV2, NOP}
import nsq.config.NSQConfig
import nsq.encoding._
import nsq.events.connection.{AddressChanged, NSQConnect}
import nsq.events.{reader, _}
import nsq.messages.{FrameType, IdentifyResponse, ResponseMessage}
import nsq.strategies.ReconnectStrategy
import nsq.strategies.retry.{NoRetryScheduler, Retry, ExponentialRetry, NoRetry}
import nsq.utils.{InitUtils, MessageUtils}

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

case class WorkerData[T](clientData: T,compression: (Compressor, Decompressor) = (NoCodingCompressor, NoCodingDecompressor),
                      connectionScheduler: Retry = NoRetryScheduler)
abstract class NSQWorkerIO[T](val address: InetSocketAddress, config: NSQConfig, initData: T,  manager:ActorRef) extends Actor
with FSM[NSQInitializationState, WorkerData[T]]
with ActorLogging {

  startWith(PreInitialization, WorkerData(clientData = initData))

  import context._

import scala.concurrent.duration.DurationInt

  val tcp = akka.io.IO(Tcp)(context.system)

  // todo watch tcpWorker
  var tcpWorker: ActorRef = null

  override def preStart() {
  }

  when(PreInitialization) {
    case Event(NSQConnect, d) =>
      if (tcpWorker != null) {
        tcpWorker ! Close
      }
      log.debug(s"Connect to $address")
      tcp ! Connect(address)
      stay()

    case Event(e: Connected, d) =>
      sender ! Register(self)
      tcpWorker = sender()
      log.debug("Connected to " + e.remoteAddress)
      writeWorker(MagicV2)
      goto(MagicBytesSent)
  }

  when(MagicBytesSent) {
    case Event(WriteAck, d) =>
      writeWorker(IdentifyConfigData(config))
      goto(Identification)
  }

  when(Identification) {
    case Event(WriteAck, d) =>
      stay()
    case Event(Received(data: ByteString), d) =>
      val msg = extractSingleMessage(data)
      msg.frame match {
        case FrameType.RESPONSE =>
          if (isHeartBeat(msg.data)) {
            writeWorker(NOP)
            log.debug("Heartbeat received during identification. NOP sent")
            stay()
          } else {
            if (msg.data.head equals '{'.toByte) {
              val iMsg = InitUtils.readIdentifyResponse(msg.data)
              val compression = iMsg match {
                case IdentifyResponse(_, _, true, _, _, _) =>
                  log.info("Level {}", msg.data.utf8String)
                  (new DeflateCompressor(iMsg.deflateLevel), new DeflateDecompressor)
                case IdentifyResponse(_, _, _, _, true, _) =>
                  (new SnappyCompressor(), new SnappyDecompressor)
                case _ => (NoCodingCompressor, NoCodingDecompressor)
              }

              if (iMsg.authRequired) {
                writeWorker(Auth(secret = config.authSecret))
                goto(Authentication)
              } else {
                goto(Initialized)
              } using WorkerData(clientData = onIdentifyResponse(iMsg), compression = compression)

            } else {
              goto(Initialized)
              //Check if the response is okay, then make the status identified
            }
          }
        case FrameType.ERROR =>
          log.error("Initialization failed during identification. Cause is {}", msg.data.utf8String)
          goto(PreInitialization) using stateAfterRestart()
        case _ =>
          log.error("Unexpected message type received during identification. Cause is {}", msg.data.utf8String)
          goto(PreInitialization) using stateAfterRestart()
      }
  }

  when(Authentication) {
    case Event(Received(data: ByteString), d) =>
      val msg = extractSingleMessage(data)
      msg.frame match {
        case FrameType.RESPONSE =>
          if (isHeartBeat(msg.data)) {
            writeWorker(NOP)
            log.debug("Heartbeat received during identification. NOP sent")
            stay()
          } else {
              goto(Initialized)
          }
        case FrameType.ERROR =>
          log.error("Initialization failed during identification. Cause is {}", msg.data.utf8String)
          goto(PreInitialization) using stateAfterRestart()
        case _ =>
          log.error("Unexpected message type received during identification. Cause is {}", msg.data.utf8String)
          goto(PreInitialization) using stateAfterRestart()
      }
  }

  whenUnhandled {
    case Event(WriteAck, _) =>
      stay()
    case Event(Reconnect, d) =>
      reconnect()
      goto(PreInitialization) using stateAfterRestart()
    case Event(e: CommandFailed, d) =>
      onConnectingCommandFailed(e)
      goto(PreInitialization) using stateAfterRestart()
    case Event(e: ConnectionClosed, d) =>
      onConnectionClosed(e)
      goto(PreInitialization) using stateAfterRestart()
  }

  onTransition {
    case (Identification | Authentication) -> PreInitialization =>
      scheduleReconnect()

  }

  initialize()

  protected def extractSingleMessage(data: ByteString): ResponseMessage = {
    MessageUtils.readUnpackedResponse(data)
  }

  protected def extractMessages(data: ByteString) : Seq[ResponseMessage] = {
    val buf = new ListBuffer[ResponseMessage]()
    readAllMessages(data, buf)
    buf
  }

  protected def encode(data: ByteString): ByteString = {
    val bs = stateData.compression._1.compressAndFlush(data)
    bs
  }

  protected def decode(data: ByteString): ByteString = stateData.compression._2.decompress(data)


  def reconnect() = {
    log.error("Reconnecting")
    self ! NSQConnect
  }

  override def postStop() {
  }

  def onConnectingCommandFailed(cmdFailed: CommandFailed) = {
    log.error(s"Host $address not available")
    scheduleReconnect()
  }

  def readSize(dataByteString: ByteString): Int = {
    if (dataByteString.length > 0)
      dataByteString.take(4).toByteBuffer.getInt
    else 0
  }

  def readBody(size: Int, dataByteString: ByteString): ByteString = {
    dataByteString.take(size)
  }

  @tailrec
  final def readAllMessages(dataByteString: ByteString, messages: ListBuffer[ResponseMessage]): Unit = {
    val size = readSize(dataByteString)
    if (size > 0) {
      val bodyData = dataByteString.drop(4)
      val data = MessageUtils.unpackResponse(readBody(size, bodyData))
      readAllMessages(bodyData.drop(size), messages += data)
    }
  }

  def onIdentifyResponse(message : IdentifyResponse) : T = stateData.clientData

  def onConnectionClosed(c: ConnectionClosed) = {
    log.warning(s"ConnectionClosed $c")
    scheduleReconnect()
  }

  def scheduleReconnect() {
      context.parent ! reader.ConnectionFailed
  }

  def clientStateAfterRestart(d: T) : T = d

  def stateAfterRestart() : WorkerData[T] = {
    WorkerData(clientData = clientStateAfterRestart(stateData.clientData))
  }

  def isHeartBeat(response: ByteString): Boolean = {
    ByteString("_heartbeat_") equals response
  }

  protected def writeWorker(msg: NSQWriteCommand) {
    val message = msg.encodedRequest
    tcpWorker ! Write(encode(message), WriteAck)
  }

  protected def writeWorker(msg: ByteString) {
    tcpWorker ! Write(encode(msg), WriteAck)
  }
}

object WriteAck extends Event

object Reconnect