package nsq.actors

import java.lang.String
import java.net.InetSocketAddress

import akka.actor.{ActorLogging, Props, ActorSystem, Actor}
import akka.util.Timeout
import nsq.messages.LookupMessages._
import spray.client.pipelining._
import spray.http.Uri.{Authority, Host, Path, Query}
import spray.http._

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

class NSQLookup extends Actor with ActorLogging{
  implicit val askTimeout: Timeout = 15.seconds
  import context.dispatcher

  import nsq.serializers.LookupUnmarshaller._

  val httpPipeline  = sendReceive ~> unmarshal[LookupResponse]
  val buildPipeline = {
    (buildRequest _).tupled
  }
  val lookupPipeline : ((InetSocketAddress, String)) => Future[LookupResponse] =
   buildPipeline  andThen
      httpPipeline

  private def uri(addr: InetSocketAddress) =
    Uri("http", Authority(Host(addr.getHostString), addr.getPort, ""), Path("/lookup"), Uri.Query.Empty, None)

  def receive: Receive = {
    case Lookup(hosts, topic) =>
      sender() ! queryLookupServer(hosts, topic)

  }
  private def queryLookupServer(hosts: Seq[InetSocketAddress], topic: String): Future[Seq[NSQClientAddress]] = {
    val dupAddresses = Future.traverse(hosts)(
      host => lookupPipeline((host, topic)).map(collectAddresses)
    )
    dupAddresses.map(_.flatten.distinct)
  }

  private def collectAddresses(r: LookupResponse) : Seq[NSQClientAddress] = {
    r.status match {
      case Good => r.data.map(_.clients).getOrElse(Seq.empty)
      case _ => Seq.empty
    }
  }

  private def buildRequest(addr: InetSocketAddress, topic: String): HttpRequest = {
    HttpRequest(HttpMethods.GET, uri(addr).withQuery(Query("topic" -> topic)))
  }
}