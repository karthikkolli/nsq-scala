package nsq.actors

import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit

import akka.actor.{Cancellable, ActorRef}
import akka.io.Tcp.Received
import akka.util.ByteString
import nsq.NSQWriteCommand
import nsq.api.connection._
import nsq.config.NSQConfig
import nsq.events._
import nsq.events.connection._
import nsq.events.reader.{MessageFinished, MessageRequeued}
import nsq.exceptions.{NoConnectionException, RequestFailedException}
import nsq.messages.{FrameType, IdentifyResponse, NSQMessage}
import nsq.utils.{MessageUtils, Operation, ProducerOperation}

import scala.collection.immutable.Queue
import scala.collection.mutable
import scala.concurrent.Promise
import scala.concurrent.duration.Duration
import scala.concurrent.duration.DurationInt

case class ProducerData(queue: Queue[ProducerOperation[ProducerActorCommand]] = Queue.empty[ProducerOperation[ProducerActorCommand]])
class NSQProducerConnection(override val address: InetSocketAddress, config: NSQConfig, topic: String, manager: ActorRef)
  extends NSQWorkerIO[ProducerData](address, config, ProducerData(), manager) {
  val OK_MESSAGE = ByteString("OK")

  when(Initialized) {
    case Event(Received(data: ByteString), d) =>
      val decompressedData = decode(data)
      val message = extractSingleMessage(decompressedData)
      message.frame match {
        case FrameType.RESPONSE =>
          if (isHeartBeat(message.data)) {
            log.debug("Received heartbeat. NOP sent")
            writeWorker(NOP)
            stay()
          } else {
            val (op, q) = d.clientData.queue.dequeue
            op.tryCompleteSuccess(message.data)
            stay() using d.copy(clientData = ProducerData(q))
          }
      }
    case Event(op: ProducerOperation[ProducerActorCommand], d) =>
      val q = d.clientData.queue.enqueue(op)
      writeWorker(op.producerCommand.content.encodedRequest)
      stay() using d.copy(clientData = ProducerData(q))
  }


  onTransition {
    case (Identification) -> Initialized => manager ! reader.ConnectionReady
    case (Initialized) -> PreInitialization =>
      stateData.clientData.queue.foreach(op => op.completeFailed(NoConnectionException))
      preStart()
  }

  override def clientStateAfterRestart(d: ProducerData) = ProducerData()

}

case class RDYData(maxRdyCount: Long = 0, maxConnRdy: Long = 0, lastRdySent: Long = 0, availableRdy: Long = 0, inFlight: Long = 0)

case class ConsumerData(queue: Queue[Operation[NSQWriteCommand]] = Queue.empty[Operation[NSQWriteCommand]], rdy: RDYData = RDYData())

class NSQConsumerConnection(override val address: InetSocketAddress, config: NSQConfig,
                            topic: String, channel: String, manager: ActorRef)
  extends NSQWorkerIO(address, config, ConsumerData(), manager) {

  import context._

  val OK_MESSAGE = ByteString("OK")

  val messageTimeout = ((config.messageTimeout.toMillis * 0.9) + 1).toInt.milliseconds

  val inFlight = mutable.HashMap[ByteString, Cancellable]()

  when(Initialized) {
    case Event(Received(data: ByteString), d) =>
      val message = extractSingleMessage(data)
      message.frame match {
        case FrameType.RESPONSE =>
          if (isHeartBeat(message.data)) {
            log.debug("Received heartbeat. NOP sent")
            writeWorker(NOP)
            stay()
          } else if (message.data equals OK_MESSAGE) {
            goto(CONNECTION_INIT) using d
          } else {
            log.error("Unknown message received in reader connection {}", message.data)
            stay()
          }
      }
  }


  when(CONNECTION_INIT) {
    case Event(Bump, d) => goto(CONNECTION_MAX) using setRdy(d.clientData.rdy.maxConnRdy)
    case Event(Backoff, d) => stay()
    case Event(SetMaxRdy(count), d) => stay() using setConnectionRdyMax(count)
    case Event(AdjustMAX, d) => stay()
    case Event(Received(data: ByteString), d) =>
      stay() using handleMessage(data)
    case Event(op: Operation[NSQWriteCommand], d) => stay() using handleOperation(op)
    case Event(t:Touch, d) => stay() using handleTouch(t)
  }

  when(CONNECTION_BACKOFF) {
    case Event(Bump, d) =>
      if (d.clientData.rdy.maxConnRdy > 0) goto(CONNECTION_ONE) using setRdy(1)
      else stay()
    case Event(SetMaxRdy(count), d) => stay() using setConnectionRdyMax(count)
    case Event(Backoff, d) => stay()
    case Event(AdjustMAX, d) => stay()
    case Event(Received(data: ByteString), d) =>
      stay() using handleMessage(data)
    case Event(op: Operation[NSQWriteCommand], d) => stay() using handleOperation(op)
    case Event(t:Touch, d) => stay() using handleTouch(t)
  }

  when(CONNECTION_ONE) {
    case Event(Bump, d) => goto(CONNECTION_MAX) using setRdy(d.clientData.rdy.maxConnRdy)
    case Event(SetMaxRdy(count), d) => stay() using setConnectionRdyMax(count)
    case Event(Backoff, d) => goto(CONNECTION_BACKOFF) using setRdy(0)
    case Event(AdjustMAX, d) => stay() using setRdy(d.clientData.rdy.maxConnRdy)
    case Event(Received(data: ByteString), d) =>
      stay() using handleMessage(data)
    case Event(op: Operation[NSQWriteCommand], d) => stay() using handleOperation(op)
    case Event(t:Touch, d) => stay() using handleTouch(t)
  }

  when(CONNECTION_MAX) {
    case Event(Bump, d) => stay()
    case Event(SetMaxRdy(count), d) => stay() using setConnectionRdyMax(count)
    case Event(Backoff, d) => goto(CONNECTION_BACKOFF) using d
    case Event(AdjustMAX, d) => stay() using setRdy(d.clientData.rdy.maxConnRdy)
    case Event(Received(data: ByteString), d) =>
      stay() using handleMessage(data)
    case Event(op: Operation[NSQWriteCommand], d) => stay() using handleOperation(op)
    case Event(t:Touch, d) => stay() using handleTouch(t)

  }

  onTransition {
    case (Identification) -> Initialized =>
      writeWorker(Subscribe(topic, channel))
    case Initialized -> CONNECTION_INIT =>
      manager ! reader.ConnectionReady
    case (Initialized | CONNECTION_INIT | CONNECTION_BACKOFF | CONNECTION_MAX | CONNECTION_ONE) -> PreInitialization =>
      manager ! reader.ConnectionClosed
    case _ -> PreInitialization => //
    case _ -> CONNECTION_MAX => //
    case _ -> CONNECTION_BACKOFF => //
    case _ -> CONNECTION_ONE => //
  }

  def setRdy(rdyCount: Long): WorkerData[ConsumerData] = {
    val clientData = stateData.clientData
    if (rdyCount < 0 || rdyCount > clientData.rdy.maxConnRdy) stateData
    else {
      writeRdy(rdyCount)
      stateData.copy(clientData = clientData.copy(rdy =
        clientData.rdy.copy(availableRdy = rdyCount, lastRdySent = rdyCount)))
    }
  }

  def setConnectionRdyMax(rdyCount: Long): WorkerData[ConsumerData] = {
    val clientData = stateData.clientData
    val maxConnRdy = math.min(rdyCount, clientData.rdy.maxRdyCount)
    self ! AdjustMAX
    stateData.copy(clientData = clientData.copy(rdy = clientData.rdy.copy(maxConnRdy = maxConnRdy)))
  }

  def writeRdy(rdyCount: Long): Unit = {
    val count = math.min(rdyCount, stateData.clientData.rdy.maxRdyCount)
    log.debug("RDY {}/{} sent", count, stateData.clientData.rdy.maxRdyCount)
    writeWorker(Ready(count))
  }

  override def onIdentifyResponse(message: IdentifyResponse): ConsumerData = {
    val clientData = stateData.clientData
    clientData.copy(rdy = clientData.rdy.copy(maxRdyCount = message.maxRdyCount))
  }

  def handleMessage(data: ByteString): WorkerData[ConsumerData] = {
    val messages = extractMessages(data)
    var queue = stateData.clientData.queue
    var rdy = stateData.clientData.rdy
    messages.foreach({ response =>
      if (isHeartBeat(response.data)) {
        writeWorker(NOP)
      } else {
        response.frame match {
          case FrameType.RESPONSE =>
            if (queue.nonEmpty) {
              val dq = queue.dequeue
              dq._1.completeSuccess(response)
              queue = dq._2
            }
          case FrameType.MESSAGE =>
            val nsqMessage = NSQMessage(self, MessageUtils.decodeMessage(response.data))
            val id = nsqMessage.id
            val scheduleMessage = Touch(id)
            val cancellable = this.context.system.scheduler.schedule(messageTimeout,
              messageTimeout, self, scheduleMessage)
            inFlight += (id -> cancellable)
            context.parent ! nsqMessage
            rdy = rdy.copy(inFlight = rdy.inFlight + 1)

          case FrameType.ERROR =>
            if (queue.nonEmpty) {
              val (op, q) = queue.dequeue
              op.completeFailed(RequestFailedException(response.data.utf8String))
              queue = q
            }
        }
      }
    })
    stateData.copy(clientData = ConsumerData(queue, rdy))
  }

  def handleOperation(op: Operation[NSQWriteCommand]): WorkerData[ConsumerData] = {
    val q = stateData.clientData.queue.enqueue(op)
    op.nsqCommand match {
      case ReQueue(id, _) =>
        inFlight.get(id).map(_.cancel())
        context.parent ! MessageRequeued
      case Finish(id) =>
        inFlight.get(id).map(_.cancel())
        context.parent ! MessageFinished
      case _ => //
    }
    writeWorker(op.nsqCommand)
    WorkerData(clientData = ConsumerData(q, stateData.clientData.rdy))
  }

  def handleTouch(t: Touch) : WorkerData[ConsumerData] = {
    val operation = Operation(t.asInstanceOf[NSQWriteCommand], Promise.apply())
    val q = stateData.clientData.queue.enqueue(operation)
    writeWorker(t.encodedRequest)
    WorkerData(clientData = ConsumerData(q, stateData.clientData.rdy))
  }

  def cleanRdyDuringRestart(rdy: RDYData) = rdy

  override def clientStateAfterRestart(c: ConsumerData) = c.copy(Queue.empty[Operation[NSQWriteCommand]], cleanRdyDuringRestart(c.rdy))
}
