package nsq.exceptions


class InvalidConfiguration(message: String) extends NSQException(message)

