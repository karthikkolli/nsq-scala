package nsq.exceptions

class NSQException(message: String) extends Exception(message)
