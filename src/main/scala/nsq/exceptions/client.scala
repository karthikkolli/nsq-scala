package nsq.exceptions


object NoConnectionException extends NSQException("No connection to server")

case class RequestFailedException( msg : String) extends NSQException(msg)

object ConnectionFailedException extends NSQException("Connection failed between nsq and client")

