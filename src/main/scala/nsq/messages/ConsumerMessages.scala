package nsq.messages

object ConsumerMessages {
  sealed trait ConsumerCommand

  object StartConsumer extends ConsumerCommand

  object StopConsumer extends ConsumerCommand
  object ConnectUsingTCP extends ConsumerCommand
  object ConnectUsingLookupd extends ConsumerCommand

  sealed trait ConsumerStatus

  object ConnectionSuccessful extends ConsumerStatus

  case class FailMessage(msg: NSQMessage) extends ConsumerCommand

  case class FinishMessage(msg: NSQMessage) extends ConsumerCommand

  case class SendReady(count: Int) extends ConsumerCommand

  trait ConsumerActorCommand

  case class SingleMessage(msg: NSQMessage) extends ConsumerActorCommand
}
