package nsq.messages

import akka.actor.ActorRef
import akka.util.ByteString
import nsq.api.connection.{Finish, ReQueue}
import nsq.utils.Operation

import scala.concurrent.{Future, Promise}
import scala.concurrent.duration.Duration

// JSON library // Combinator syntax

case class WriteMsg(bytes: ByteString)

case class IdentifyResponse(maxRdyCount: Long, tlsV1: Boolean, deflate: Boolean,
                            deflateLevel : Int, snappy: Boolean, authRequired: Boolean)

object FrameType extends Enumeration {
  type FrameType = Value
  val RESPONSE = Value(0)
  val ERROR = Value(1)
  val MESSAGE = Value(2)
}

case class ResponseMessage(frame: FrameType.FrameType, data: ByteString)

object NSQMessage {
  def apply(conn: ActorRef, msg: NSQMessageData) : NSQMessage =
    NSQConnectionMessage(conn, msg.id, msg.body, msg.timestamp, msg.attempts)
}
trait NSQMessage {
  val id: ByteString
  val body: ByteString
  val timestamp: Long
  val attempts: Short

  def finish : Future[ResponseMessage]
  def requeue(time: Duration): Future[ResponseMessage]
}

case class NSQMessageData(id: ByteString, body: ByteString, timestamp: Long, attempts: Short)

final case class NSQConnectionMessage(connection: ActorRef, id: ByteString, body: ByteString, timestamp: Long, attempts: Short)
  extends NSQMessage{

  override def finish : Future[ResponseMessage] = {
    val promise = Promise[ResponseMessage]()
    val operation = Operation(Finish(id), promise)
    connection ! operation
    promise.future
  }

  override def requeue(time: Duration) : Future[ResponseMessage] = {
    val promise = Promise[ResponseMessage]()
    val operation = Operation(ReQueue(id, time), promise)
    connection ! operation
    promise.future
  }
}

