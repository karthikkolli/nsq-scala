package nsq.messages

import java.net.InetSocketAddress

object LookupMessages {

  case class LookupAddress(addr: String, port: Int)

  case class Lookup(addr: Seq[InetSocketAddress], topic: String)

  sealed trait LookupStatus

  case object ServerNotReachable extends LookupStatus

  case object TopicNotFound extends LookupStatus

  case object BadJson extends LookupStatus

  case object Good extends LookupStatus

  case class LookupData(channels: Seq[String], clients: Seq[NSQClientAddress])

  case class LookupResponse(status: LookupStatus, data: Option[LookupData])

  case class NSQClientAddress(addr: String, port: Int, version: String)
}
