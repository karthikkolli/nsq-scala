package nsq.utils

import java.util.regex.Matcher

import scala.util.matching.Regex

object ConnectionUtils {
  var validRegex = """^[.a-zA-Z0-9_-]+(#ephemeral)?""".r
  def isValidTopicName(topic: String): Boolean = {
    topic.length > 0 && topic.length < 65 && validRegex.findFirstIn(topic).isDefined
  }

  def isValidChannelName(channel: String) : Boolean = {
    channel.length > 0 && channel.length < 65 && validRegex.findFirstIn(channel).isDefined
  }
}