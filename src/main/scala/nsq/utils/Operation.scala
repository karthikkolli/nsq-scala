package nsq.utils

import akka.util.ByteString
import nsq.NSQWriteCommand
import nsq.actors.ProducerActorCommand
import nsq.events.{SendFailed, SendSuccessful, ProducerResponse}
import nsq.messages.ResponseMessage
import nsq.protocol.NSQReply

import scala.concurrent.Promise

case class Operation[T <: NSQWriteCommand](nsqCommand: T, promise: Promise[ResponseMessage]) {

  def completeSuccess(nsqReply: ResponseMessage) = {
    promise.success(nsqReply)
  }

  def tryCompleteSuccess(nsqReply: NSQReply) = {
    val v = MessageUtils.readUnpackedResponse(nsqReply.asInstanceOf[ByteString])
    promise.trySuccess(v)
  }

  def completeSuccessValue(value: ResponseMessage) = promise.success(value)

  def completeFailed(t: Throwable) = promise.failure(t)
}

case class ProducerOperation[T <: ProducerActorCommand](producerCommand: T, promise: Promise[ProducerResponse]) {

  def completeSuccessValue(value: ProducerResponse) = {
    println("Returning promise value")
    promise.success(value)
  }

  val OK = ByteString("OK")

  def tryCompleteSuccess(message: ByteString) = {
    message match {
      case OK => completeSuccessValue(SendSuccessful(producerCommand.id))
      case _ => completeSuccessValue(SendFailed(producerCommand.id, message.utf8String))
    }

  }

  def completeFailed(t: Throwable) = promise.failure(t)
}
