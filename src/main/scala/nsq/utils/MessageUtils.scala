package nsq.utils

import akka.util.ByteString
import nsq.messages._

object MessageUtils {

  def readUnpackedResponse(dataByteString: ByteString): ResponseMessage = {
      val response = readResponse(dataByteString)
      unpackResponse(response)
  }

  // ReadResponse is a client-side utility function to read from the supplied Reader
  // according to the NSQ protocol spec:
  //
  //    [x][x][x][x][x][x][x][x]...
  //    |  (int32) || (binary)
  //    |  4-byte  || N-byte
  //    ------------------------...
  //        size       data
  private def readResponse(dataByteString: ByteString): ByteString = {
    val size = dataByteString.take(4).toByteBuffer.getInt
    val data = dataByteString.slice(4,size+4)
    data
  }

  // UnpackResponse is a client-side utility function that unpacks serialized data
  // according to NSQ protocol spec:
  //
  //    [x][x][x][x][x][x][x][x]...
  //    |  (int32) || (binary)
  //    |  4-byte  || N-byte
  //    ------------------------...
  //      frame ID     data
  //
  // Returns a Tuple2 of: frame type, data

  def unpackResponse(response: ByteString): ResponseMessage = {
    val (frame, data) = response.splitAt(4)
    ResponseMessage(FrameType.apply(frame.toByteBuffer.getInt), data)
  }

  def decodeMessage(data: ByteString): NSQMessageData = {
    NSQMessageData(data.slice(10, 26), data.drop(26), data.take(8).toByteBuffer.getLong, data.slice(8,10).toByteBuffer.getShort)
  }
}