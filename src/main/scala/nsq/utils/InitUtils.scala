package nsq.utils

import java.util.zip.Inflater

import akka.util.{ByteString, ByteStringBuilder}
import nsq.messages.IdentifyResponse
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

object NSQJsonFormats {
  implicit val identifyReads = (
    (__ \ "max_rdy_count").read[Long] and
      (__ \ "tls_v1").read[Boolean] and
      (__ \ "deflate").read[Boolean] and
      (__ \ "deflate_level").read[Int] and
      (__ \ "snappy").read[Boolean] and
      (__ \ "auth_required").read[Boolean]
    )(IdentifyResponse.apply _)
}

object InitUtils {
  def readIdentifyResponse(data: ByteString): IdentifyResponse = {
    import NSQJsonFormats.identifyReads
    Json.parse(data.utf8String).as[IdentifyResponse]
  }

  def inflate(data: ByteString): Try[ByteString] = {
    val buffer = data.toArray
    inflate(buffer, 0)
  }

  def inflate(buffer: Array[Byte], offset: Int): Try[ByteString] = {
    val inflater = new Inflater()
    val outputBuf = new Array[Byte](1024) // use a working buffer of size 1 KB)
    val output = ByteString.newBuilder
    try {
      if (buffer.length > offset) {
        inflater.setInput(buffer, offset, buffer.length - offset)
        drain(inflater, outputBuf, output)
        if (inflater.needsDictionary) Failure(new Exception("ZLIB dictionary missing"))
        Success(output.result())
      } else Success(ByteString.empty)
    } catch {
      case e: Throwable => new Failure(e)
    }

  }

  @tailrec
  private def drain(inflater: Inflater, outputBuf: Array[Byte], output: ByteStringBuilder): Unit = {
    val len = inflater.inflate(outputBuf)
    if (len > 0) {
      output ++= outputBuf
      drain(inflater, outputBuf, output)
    }
  }

}