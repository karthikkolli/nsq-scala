package nsq.strategies.retry

import scala.concurrent.duration.{FiniteDuration}

class CounterBasedRetry(counter: Int = 3, interval: FiniteDuration)
  extends Retry {
  var inCounter = counter

  override def nextReconnect(): FiniteDuration = interval

  override def reset(): Unit = {
    inCounter = 0
  }

  override def failed(): Unit = {
    inCounter += 1
  }

  override def hasNext: Boolean = inCounter < counter
}
