package nsq.strategies.retry

import scala.concurrent.duration.{FiniteDuration, Duration}

abstract class Retry {
  def hasNext: Boolean
  def nextReconnect() : FiniteDuration
  def reset() : Unit
  def failed() : Unit
}
