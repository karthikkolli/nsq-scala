package nsq.strategies.retry

import scala.concurrent.duration.FiniteDuration

class FixedDurationRetry(duration: FiniteDuration) extends Retry {

  override def reset(): Unit = {}

  override def failed(): Unit = {}

  override def hasNext: Boolean = true

  override def nextReconnect(): FiniteDuration = duration
}
