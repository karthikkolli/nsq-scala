package nsq.strategies.retry

import scala.concurrent.duration.{FiniteDuration, DurationLong}

class NoRetry()
  extends Retry{

  override def reset(): Unit = {}

  override def failed(): Unit = {}

  override def hasNext: Boolean = false

  override def nextReconnect(): FiniteDuration = 10.minutes
}

object NoRetryScheduler extends NoRetry

