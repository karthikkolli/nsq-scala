package nsq.strategies.retry

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.{Duration, DurationLong, FiniteDuration}

class ExponentialRetry(minInterval: FiniteDuration, maxInterval: FiniteDuration, ratio: Double = 0.25,
                       shortLength: Int = 10, longLength: Int = 250)
  extends Retry{

  val intervalDelta = maxInterval - minInterval
  val maxShortTimer = intervalDelta * ratio
  val maxLongTimer = intervalDelta * (1 - ratio)
  val shortUnit = maxShortTimer / shortLength
  val longUnit = maxLongTimer / longLength

  var shortInterval: Duration = 0.seconds
  var longInterval: Duration = 0.seconds

  override def nextReconnect(): FiniteDuration = {
    FiniteDuration((minInterval + shortInterval + longInterval).toMillis,
      TimeUnit.MILLISECONDS)
  }

  override def reset(): Unit = {
    shortInterval = 0.seconds
    longInterval = 0.seconds
  }

  override def failed(): Unit = {
    shortInterval = shortInterval + shortUnit
    shortInterval = if(shortInterval.lteq(maxShortTimer)) shortInterval else maxShortTimer
    longInterval =  longInterval + longUnit
    longInterval = if(longInterval.lteq(maxLongTimer)) longInterval else maxLongTimer
  }

  override def hasNext: Boolean = true
}
