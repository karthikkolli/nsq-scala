package nsq

package object strategies {
  object RetryStrategy extends Enumeration {
    type RetryStrategy = Value
    val NONE = Value(0)
    val COUNTER_RETRY = Value(1)
    val TIME_RETRY = Value(2)
  }

  object ReconnectStrategy extends Enumeration {
    type ReconnectStrategy = Value
    val NONE = Value(0)
    val FIXED_RECONNECT = Value(1)
    val EXPONENTIAL_RECONNECT = Value(2)
  }
}
