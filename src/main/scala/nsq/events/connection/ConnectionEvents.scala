package nsq.events.connection

import java.net.InetSocketAddress


sealed trait NSQConnectionEvents

object NSQConnect extends NSQConnectionEvents

object NSQClose extends NSQConnectionEvents

case class AddressChanged(addr: InetSocketAddress) extends NSQConnectionEvents

sealed trait RDYEvents

object Backoff extends RDYEvents

object Bump extends RDYEvents

object BumpNew extends RDYEvents

object AdjustMAX extends RDYEvents

object GetRdyCount extends RDYEvents

object GetInFlightCount extends RDYEvents

final case class SetMaxRdy(count : Long) extends RDYEvents

final case class SetMaxRdyAndBump(count : Long) extends RDYEvents
