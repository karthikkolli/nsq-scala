package nsq.events.connection

import nsq.events.NSQInitializationState

sealed trait RDYState extends NSQInitializationState

object CONNECTION_INIT extends RDYState

object CONNECTION_BACKOFF extends RDYState

object CONNECTION_ONE extends RDYState

object CONNECTION_MAX extends RDYState

