package nsq.events.reader

import akka.actor.ActorRef
import nsq.messages.NSQMessage

sealed trait ReaderEvents
object Backoff extends ReaderEvents
object Success extends ReaderEvents
object TryRead extends ReaderEvents
object Pause extends ReaderEvents
object UnPause extends ReaderEvents

object Error extends ReaderEvents
object Message extends ReaderEvents
object Discard extends ReaderEvents
case class AddConnection(conn: ActorRef) extends ReaderEvents
case class RemoveConnection(conn: ActorRef) extends ReaderEvents
case class MessageReceived(message: NSQMessage) extends ReaderEvents
object MessageFinished extends ReaderEvents
object MessageRequeued extends ReaderEvents
case class NewConnection(conn:ActorRef) extends ReaderEvents
object ConnectionReady extends ReaderEvents
object ConnectionClosed extends ReaderEvents
object ConnectionFinished extends ReaderEvents
object ConnectionFailed extends ReaderEvents