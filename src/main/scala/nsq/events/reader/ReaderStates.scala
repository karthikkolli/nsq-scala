package nsq.events.reader

sealed trait ReaderState

object READER_ZERO extends ReaderState
object READER_PAUSE extends ReaderState
object READER_TRYONE extends ReaderState
object READER_MAX extends ReaderState
object READER_BACKOFF extends ReaderState
