package nsq

import java.util.UUID
import java.util.concurrent.Future

import akka.actor.ActorRef
import akka.util.ByteString

package object events {

  sealed trait NSQConnectionEvent

  object ConnectionSuccesful extends NSQConnectionEvent

  trait NSQInitializationState

  object PreInitialization extends NSQInitializationState

  object MagicBytesSent extends NSQInitializationState

  object Identification extends NSQInitializationState

  object IdentifyRequired extends NSQInitializationState

  object IdentifySent extends NSQInitializationState

  object IdentifyFailed extends NSQInitializationState

  object IdentifySuccessful extends NSQInitializationState

  object IdentifyInProgress extends NSQInitializationState

  object NoCompressionRequired extends NSQInitializationState

  object DeflateRequired extends NSQInitializationState

  object DeflateFailed extends NSQInitializationState

  object DeflateSuccessful extends NSQInitializationState

  object SnappyRequired extends NSQInitializationState

  object SnappyFailed extends NSQInitializationState

  object SnappySuccessful extends NSQInitializationState

  object Authentication extends NSQInitializationState

  object CheckAuth extends NSQInitializationState

  object Initialized extends NSQInitializationState

  object WaitingForConfirmation extends NSQInitializationState

  object InitializationFailed extends NSQInitializationState

  object Subscribed extends NSQInitializationState

  sealed trait NSQAuthenticationEvent

  object AuthNotChecked extends NSQAuthenticationEvent

  object AuthNotRequired extends NSQAuthenticationEvent

  object AuthPending extends NSQAuthenticationEvent

  object AuthFailed extends NSQAuthenticationEvent

  object AuthSuccessful extends NSQAuthenticationEvent

//  sealed trait NSQEvents
//
//
//
//  object Respawn extends NSQEvents
//
//  object Redistribute extends NSQEvents
//
//  object MessageProcessed extends NSQEvents

  import scala.collection.immutable.{ Queue => Q }
  sealed trait Data

  case object Uninitialized extends Data

  case class SentMessages(map : Set[UUID]) extends Data

  sealed trait ProducerResponse

  case class SendSuccessful(id: UUID) extends ProducerResponse

  case class SendFailed(id: UUID, message: String) extends ProducerResponse

  case class SendFailedMultiple(ar: Seq[UUID]) extends ProducerResponse

  class ProducerException(message:String) extends Exception(message)
  case class MessageTooBigException(len: Long) extends ProducerException( "Message size " + len + " too big. Max allowed is 2500000")

}


