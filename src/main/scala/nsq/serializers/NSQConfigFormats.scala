package nsq.serializers

import nsq.config.{NSQConfig, CompressionType}
import play.api.libs.json.{Json, JsValue, Writes}

object NSQConfigFormats {
  implicit val configFormats = new Writes[NSQConfig] {
    override def writes(o: NSQConfig): JsValue = {
      val config = Json.obj(
        "client_id" -> o.clientInfo.clientId,
        "hostname" -> o.clientInfo.hostname,
        "feature_negotiation" -> true,
        "heartbeat_interval" -> o.heartbeatInterval.toMillis,
        "output_buffer_size" -> o.outputBufferSize,
        "output_buffer_timeout" -> o.outputBufferTimeout.toMillis,
        "user_agent" -> o.userAgent,
        "msg_timeout" -> o.messageTimeout.toMillis,
        "sample_rate" -> o.sampleRate
      )

      val compressionConfig = o.compressionType match {
        case CompressionType.DEFLATE =>
          println(o.compressionLevel)
          Json.obj(
            "deflate" -> true,
            "deflate_level" -> o.compressionLevel.id
          )
        case CompressionType.SNAPPY =>
          Json.obj(
            "snappy" -> true
          )
        case _ => Json.obj()
      }

      val tlsConfig = o.tlsOptions match {
        case Some(_) => Json.obj(
          "tls_v1" -> true
        )
        case None => Json.obj()
      }

      config ++ compressionConfig ++ tlsConfig
    }
  }
}