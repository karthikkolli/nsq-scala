package nsq.serializers

import nsq.messages.LookupMessages._
import play.api.libs.functional.syntax._
import play.api.libs.json._
import spray.http.HttpEntity
import spray.http.MediaTypes._
import spray.httpx.unmarshalling.Unmarshaller


object LookupUnmarshaller {
  implicit private val nsqclientAddressReadFormat : Reads[NSQClientAddress] = (
    (JsPath \ "remote_address").read[String].map(addr => addr.split(":").apply(0)) and
      (JsPath \ "tcp_port").read[Int] and
      (JsPath \ "version").read[String]
    )(NSQClientAddress.apply _)

  implicit private val lookupDataReadFormat : Reads[LookupData] = (
    (JsPath \ "channels").read[Seq[String]] and
      (JsPath \ "producers").read[Seq[NSQClientAddress]]
  )(LookupData.apply _)

  implicit private val lookupResponseReadFormat: Reads[LookupResponse] = (
      (JsPath \ "status_txt").read[String] map {
        case "OK" => Good
        case "TOPIC_NOT_FOUND" => TopicNotFound
        case default => throw new RuntimeException("Unknown msg " + default)
      } and
    (JsPath \ "data").readNullable[LookupData]
  )(LookupResponse.apply _)

  implicit val LookupResponseUnmarshaller = Unmarshaller[LookupResponse](`application/json`){
    case HttpEntity.NonEmpty(contentType, data) =>
      val json = Json.parse(data.asString)
      lookupResponseReadFormat.reads(json) getOrElse LookupResponse(BadJson, None)
  }
}
