package nsq

import java.net.InetSocketAddress

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.ByteString
import nsq.api.connection.IdentifyConfigData
import nsq.config.{ClientInfo, NSQConfig}
import org.scalatest._

import scala.concurrent.duration.DurationInt


class NSQConfigSpec
  extends WordSpec
  with Matchers{

  val address = new InetSocketAddress("localhost", 4150)

  "NSQConfig creation" must {

    "succeed with default data and nsqd address" in {
      noException should be thrownBy NSQConfig(nsqdTCPAddresses = Seq(address))
    }

    "succeed with default data and lookupd address" in {
      noException should be thrownBy NSQConfig(lookupHttpAddresses = Seq(address))
    }

    "fail with heartbeat less than 1 second" in {
      a [AssertionError] should be thrownBy NSQConfig(nsqdTCPAddresses = Seq(address), heartbeatInterval = 999.milliseconds)
    }

    "fail with output buffersize less than 64" in {
      a [AssertionError] should be thrownBy NSQConfig(nsqdTCPAddresses = Seq(address), outputBufferSize = 63)
    }

    "fail with outputBufferTimeout less than 1 milli second" in {
      a [AssertionError] should be thrownBy NSQConfig(nsqdTCPAddresses = Seq(address), outputBufferTimeout = 999.nanoseconds)
    }

    "fail with negative sample rate" in {
      a [AssertionError] should be thrownBy NSQConfig(nsqdTCPAddresses = Seq(address), sampleRate = -1)
    }

    "fail with sample rate greater than 99" in {
      a [AssertionError] should be thrownBy NSQConfig(nsqdTCPAddresses = Seq(address), sampleRate = 100)
    }

    "fail with poll jitter greater than or equal 1" in {
      a [AssertionError] should be thrownBy NSQConfig(nsqdTCPAddresses = Seq(address), lookupdPollJitter = 1.0)
    }

    "fail with negative poll jitter" in {
      a [AssertionError] should be thrownBy NSQConfig(nsqdTCPAddresses = Seq(address), lookupdPollJitter = 1.0)
    }

    "fail with no tcp address and no lookup addresses" in {
      a [AssertionError] should be thrownBy NSQConfig()
    }

    "both tcp address and lookup address" in {
      a [AssertionError] should be thrownBy NSQConfig(lookupHttpAddresses = Seq(address), nsqdTCPAddresses = Seq(address))
    }
  }
}
