package nsq

import java.net.InetSocketAddress

import akka.actor.ActorSystem
import akka.actor.FSM.{Transition, CurrentState, SubscribeTransitionCallBack}
import akka.io.Tcp._
import akka.testkit._
import akka.util.ByteString
import nsq.TestUtils._
import nsq.actors.{NSQWorkerIO, WriteAck}
import nsq.api.connection.IdentifyConfigData
import nsq.config.{ClientInfo, NSQConfig}
import nsq.encoding._
import nsq.events.connection.NSQConnect
import nsq.events._
import nsq.messages.{IdentifyResponse, ResponseMessage, FrameType}
import org.scalatest.{Matchers, WordSpecLike}

import scala.concurrent.Await

class NSQWorkerIOConnectionSpecs(_system:ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers {
  def this() = this(ActorSystem("test"))

  val address = new InetSocketAddress("localhost", 4150)
  val nsqConfig = NSQConfig(clientInfo = ClientInfo("TestId", "TestHost"),
    nsqdTCPAddresses = Seq[InetSocketAddress](address))

  trait scope {
    val tcpProbe = TestProbe()
    val managerProbe = TestProbe()
    val workerProbe = TestProbe()
    case class TestData()
    class BasicConnection extends NSQWorkerIO[TestData](address, nsqConfig, TestData() ,managerProbe.ref) {
      override val tcp = tcpProbe.ref
      tcpWorker = workerProbe.ref
      //Always should have the default implementation of Initialized
      when(Initialized) {
        case Event(Received(message), d) => stay()
      }
    }
    val workerFSM = TestFSMRef(new BasicConnection)
  }


  "During initialization connection should" should{
    "have initial state as pre initialization"  in {
      new scope {
        assert(workerFSM.stateName == PreInitialization)
      }

    }
    "send connect message to the tcp actor" in {
      new scope {
        workerFSM ! NSQConnect
        tcpProbe.expectMsg(Connect(address))
      }

    }
    "send magic bytes and goto state MagicBytesSent on receiving valid connected message in preinitialization" in {
      new scope {
        workerFSM.setState(PreInitialization)
        val dummyAddress = new InetSocketAddress("127.0.0.1", 1111)
        workerFSM.tell(Connected(dummyAddress, dummyAddress), tcpProbe.ref)
//      tcp worker receives a register message
        tcpProbe.expectMsg(Register(workerFSM))
        val magicBytes = ByteString("  V2")
        tcpProbe.expectMsg(Write(magicBytes, WriteAck))
        assert(workerFSM.stateName == MagicBytesSent)
      }
    }

    "go to state Identification on receiving WriteAck in state MagicBytesSent" in {
      new scope {
        workerFSM.setState(MagicBytesSent)
        workerFSM ! SubscribeTransitionCallBack(testActor)
        workerFSM ! WriteAck
        expectMsg(CurrentState(workerFSM, MagicBytesSent))
        val identifyData = IdentifyConfigData(nsqConfig).encodedRequest
        workerProbe.expectMsg(Write(identifyData, WriteAck))
        expectMsg(Transition(workerFSM, MagicBytesSent, Identification))
      }
    }

    "go to initialized on receiving non json data from server" in {
      new scope {
        workerFSM.setState(Identification)
        import TestUtils._
        val message = packMessage(ResponseMessage(FrameType.RESPONSE, ByteString("OK")))
        workerFSM ! SubscribeTransitionCallBack(testActor)
        workerFSM ! Received(message)
        expectMsg(CurrentState(workerFSM, Identification))
        expectMsg(Transition(workerFSM, Identification, Initialized))
      }
    }

    "go to initialized on receiving no compression flags from server" in {
      new scope {
        val identifyResponse = IdentifyResponse(maxRdyCount = 100, tlsV1 = false,
          deflate = false, deflateLevel = 0,
          snappy = false, authRequired = false)
        workerFSM.setState(Identification)
        import TestUtils._
        val iResp = identifyWrites.writes(identifyResponse)
        val message = packMessage(ResponseMessage(FrameType.RESPONSE, ByteString(iResp.toString())))
        workerFSM ! SubscribeTransitionCallBack(testActor)
        workerFSM ! Received(message)
        expectMsg(CurrentState(workerFSM, Identification))
        expectMsg(Transition(workerFSM, Identification, Initialized))
        assert(workerFSM.stateData.compression == (NoCodingCompressor, NoCodingDecompressor))
      }
    }

    "set snappy compressor and decompressor on receiving snappy flags from server" in {
      new scope {
        val identifyResponse = IdentifyResponse(maxRdyCount = 100, tlsV1 = false,
          deflate = false, deflateLevel = 0,
          snappy = true, authRequired = false)
        workerFSM.setState(Identification)
        import TestUtils._
        val iResp = identifyWrites.writes(identifyResponse)
        val message = packMessage(ResponseMessage(FrameType.RESPONSE, ByteString(iResp.toString())))
        workerFSM ! SubscribeTransitionCallBack(testActor)
        workerFSM ! Received(message)
        expectMsg(CurrentState(workerFSM, Identification))
        expectMsg(Transition(workerFSM, Identification, Initialized))
        assert(workerFSM.stateData.compression._1.isInstanceOf[SnappyCompressor])
        assert(workerFSM.stateData.compression._2.isInstanceOf[SnappyDecompressor])
      }
    }

    "set deflate compressor and decompressor on receiving deflate compression flags from server" in {
      new scope {
        val identifyResponse = IdentifyResponse(maxRdyCount = 100, tlsV1 = false,
          deflate = true, deflateLevel = 5,
          snappy = false, authRequired = false)
        workerFSM.setState(Identification)
        import TestUtils._
        val iResp = identifyWrites.writes(identifyResponse)
        val message = packMessage(ResponseMessage(FrameType.RESPONSE, ByteString(iResp.toString())))
        workerFSM ! SubscribeTransitionCallBack(testActor)
        workerFSM ! Received(message)
        expectMsg(CurrentState(workerFSM, Identification))
        expectMsg(Transition(workerFSM, Identification, Initialized))
        assert(workerFSM.stateData.compression._1.isInstanceOf[DeflateCompressor])
        assert(workerFSM.stateData.compression._2.isInstanceOf[DeflateDecompressor])
      }
    }

    "go to authentication state on receiving authentication required flag from server" in {
      new scope {
        val identifyResponse = IdentifyResponse(maxRdyCount = 100, tlsV1 = false,
          deflate = true, deflateLevel = 5,
          snappy = false, authRequired = true)
        workerFSM.setState(Identification)
        import TestUtils._
        val iResp = identifyWrites.writes(identifyResponse)
        val message = packMessage(ResponseMessage(FrameType.RESPONSE, ByteString(iResp.toString())))
        workerFSM ! SubscribeTransitionCallBack(testActor)
        workerFSM ! Received(message)
        expectMsg(CurrentState(workerFSM, Identification))
        expectMsg(Transition(workerFSM, Identification, Authentication))
      }
    }
  }


}
