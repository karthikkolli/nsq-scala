package nsq

import java.net.InetSocketAddress

import akka.pattern.ask
import akka.actor.{Props, ActorSystem, Actor}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import akka.util.Timeout
import nsq.actors.NSQLookup
import nsq.messages.LookupMessages
import nsq.messages.LookupMessages.{NSQClientAddress, Lookup, LookupResponse}
import org.scalatest._
import spray.http._
import akka.testkit.{TestKit, TestActorRef}
import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Future, Promise}
import scala.util.Success

class LookupSpec(_system: ActorSystem) extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterEach
  with BeforeAndAfterAll{

  def this() = this(ActorSystem("LookupSpec"))

  override def afterAll() {
    TestKit.shutdownActorSystem(system)
  }

  val lookup1 = new InetSocketAddress("127.0.0.1", 4161)
  val lookup2 = new InetSocketAddress("127.0.0.1", 5161)
  val lookup3 = new InetSocketAddress("127.0.0.1", 6161)

  val nsq1 = new InetSocketAddress("172.17.42.7", 4150)
  val nsq2 = new InetSocketAddress("172.17.42.8", 4150)
  val nsq3 = new InetSocketAddress("172.17.42.9", 4150)


  val topic = "sample_topic"
  case class TestNSQData(address: InetSocketAddress, topics: Seq[String], version: String)
  val nsqData1 = TestNSQData(nsq1, Seq(topic), "0.3.0")
  val nsqData2 = TestNSQData(nsq2, Seq(topic), "0.2.9")
  val nsqData3 = TestNSQData(nsq3, Seq(topic), "0.3.0")

  trait scope {

    val lookupTopics = scala.collection.mutable.Map[InetSocketAddress, Map[String, Map[InetSocketAddress, String]]]()
    val badTopics = scala.collection.mutable.Map[InetSocketAddress, Set[String]]()

    def registerWithLookupd(lookupAddress: InetSocketAddress, nsq: TestNSQData) = {
      var lookupMap = lookupTopics.getOrElse(lookupAddress, Map.empty)
      nsq.topics.foreach(topic => {
        val topicAddresses = lookupMap.getOrElse(topic, Map.empty) + (nsq.address -> nsq.version)
        lookupMap += topic -> topicAddresses
      })
      lookupTopics += lookupAddress -> lookupMap
    }

    def mockPipeline(tu: (InetSocketAddress,String)): Future[LookupResponse] = {
      val topic = tu._2
      val address = tu._1

      val producers = lookupTopics.get(address).flatMap(m => m.get(topic))
      producers match {
        case Some(v) =>
          val lookupData = LookupMessages.LookupData(Seq.empty, v.keySet.toSeq.map(x => LookupMessages.NSQClientAddress(x.getHostString, x.getPort, v.get(x).get)))
          Future.successful(LookupResponse(status = LookupMessages.Good, data = Some(lookupData)))
        case None =>
            Future.successful(LookupResponse(status = LookupMessages.TopicNotFound, data = None))
      }
    }

    class TestNSQLookup extends NSQLookup {
      override val lookupPipeline = mockPipeline _
    }
    implicit val timeout = Timeout(5.seconds)


    val lookupActor = TestActorRef(new TestNSQLookup)
  }

  "Querying a single lookupd for a topic" should {
    "return an empty list if nsqd servers are registered for that topic" in {
      new scope {
        lookupActor ! Lookup(Seq(lookup1), topic)
        expectMsg(2.seconds, Nil)
      }
    }

    "return a list of nsqd servers if server is registered for that topic" in {
      new scope {
        registerWithLookupd(lookup1, nsqData1)
        lookupActor ! Lookup(Seq(lookup1), topic)
        expectMsg(2.seconds, Seq(NSQClientAddress(nsqData1.address.getHostName,nsqData1.address.getPort, nsqData1.version)))
      }
    }
  }

  "Querying multiple lookupd servers for a topic" should {
    "combine results from multiple lookupd" in {
      new scope {
        registerWithLookupd(lookup1, nsqData1)
        registerWithLookupd(lookup2, nsqData2)
        registerWithLookupd(lookup3, nsqData3)

        lookupActor ! Lookup(Seq(lookup1, lookup2, lookup3), topic)
        val addr = expectMsgClass(classOf[Seq[NSQClientAddress]])
        assert(addr.length == 3)
      }
    }
    "dedupe combined results from multiple lookupd" in {
      new scope {
        registerWithLookupd(lookup1, nsqData1)
        registerWithLookupd(lookup2, nsqData1)
        registerWithLookupd(lookup3, nsqData1)
        lookupActor ! Lookup(Seq(lookup1, lookup2), topic)
        val addr = expectMsgClass(classOf[Seq[NSQClientAddress]])
        assert(addr.length == 1)
      }
    }
  }
}
