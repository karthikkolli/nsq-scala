package nsq

import akka.util.ByteString
import nsq.messages.{IdentifyResponse, ResponseMessage}
import play.api.libs.json._

object TestUtils {
  def packMessage(msg:ResponseMessage) : ByteString = {
    val packedResponse = ByteStringSerializer.integer2Byte(msg.frame.id) ++ msg.data
    ByteString(ByteStringSerializer.integer2Byte(packedResponse.size) ++ packedResponse)
  }
  import play.api.libs.json._
  // imports required functional generic structures
  import play.api.libs.functional.syntax._
  val identifyWrites = new Writes[IdentifyResponse]{
    def writes(c: IdentifyResponse): JsValue = {
      Json.obj(
        "max_rdy_count" -> c.maxRdyCount,
        "tls_v1" -> c.tlsV1,
        "deflate" -> c.deflate,
        "deflate_level" -> c.deflateLevel,
        "snappy" -> c.snappy,
        "auth_required" -> c.authRequired
      )
    }
  }
}
