package nsq

import java.math.BigInteger
import java.net.InetSocketAddress

import akka.actor.ActorSystem
import akka.testkit._
import akka.util.ByteString
import nsq.api.connection._
import nsq.api.{MultiPublish, Publish}
import nsq.config.{ClientInfo, NSQConfig}
import nsq.utils.MessageUtils
import org.scalatest._

import scala.concurrent.duration.DurationInt


class ConnectionSpec(_system: ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll{

  def this() = this(ActorSystem("ConnectionSpec"))

  override def afterAll() {
    TestKit.shutdownActorSystem(system)
  }

  val address = new InetSocketAddress("localhost", 4150)
  val nsqConfig = NSQConfig(clientInfo = ClientInfo("TestId", "TestHost"),
    nsqdTCPAddresses = Seq[InetSocketAddress](address))

  "A Connection Worker" must {
    "construct an identity message" in {
      val msg = ByteString("{\"client_id\":\"TestId\",\"hostname\":\"TestHost\",\"feature_negotiation\":true," +
        "\"heartbeat_interval\":30000,\"output_buffer_size\":16384,\"output_buffer_timeout\":250," +
        "\"user_agent\":\"nsq-scala "+nsq.VERSION+"\",\"msg_timeout\":10000,\"sample_rate\":0}")
      IdentifyConfigData(nsqConfig).encodedRequest should be
      {ByteString("IDENTIFY\n") ++ ByteStringSerializer.integer2Byte(msg.length) ++ msg}
    }

    "construct an identity message with heartbeat of 10 seconds" in {
      val msg = ByteString("{\"client_id\":\"TestId\",\"hostname\":\"TestHost\",\"feature_negotiation\":true," +
        "\"heartbeat_interval\":10000,\"output_buffer_size\":16384,\"output_buffer_timeout\":250," +
        "\"user_agent\":\"nsq-scala "+nsq.VERSION+"\",\"msg_timeout\":10000,\"sample_rate\":0}")
      val heartbeatConfig = nsqConfig.copy(heartbeatInterval = 10.seconds)
      IdentifyConfigData(heartbeatConfig).encodedRequest should be
      {ByteString("IDENTIFY\n") ++ ByteStringSerializer.integer2Byte(msg.length) ++ msg}
    }

    "construct an version message" in {
      MagicV2.encodedRequest should be
      {ByteString("  V2")}
    }

    "construct an auth message" in {
      val secret = "secret"
      Auth(secret).encodedRequest should be
      {ByteString("REGISTER\n") ++ ByteString(0,0,0,6) ++ ByteString(secret)}
    }

    "construct an subscribe message" in {
      Subscribe("TestTopic", "TestChannel").encodedRequest should be
      {ByteString("SUB TestTopic TestChannel\n")}
    }

    "construct an subscribe message with unicode topic" in {
      Subscribe("こんにちは", "こんにちは").encodedRequest should be
      {ByteString("SUB こんにちは こんにちは\n")}
    }

    "construct an ready message" in {
      Ready(1).encodedRequest should be
      {ByteString("RDY 1\n")}
    }

    "construct an finish message" in {
      val idBytes = ByteString("test")
      Finish(idBytes).encodedRequest should be
      {ByteString("FIN test\n")}
    }

    "reque a message without timeout" in {
      val idBytes = ByteString("test")
      ReQueue(idBytes).encodedRequest should be
      {ByteString("REQ test 0\n")}
    }

    "reque a message with timeout of 5 seconds" in {
      val idBytes = ByteString("test")
      ReQueue(idBytes, 5.seconds).encodedRequest should be
      {ByteString("REQ test 5\n")}
    }

    "a nop message" in {
      NOP.encodedRequest should be
      {ByteString("NOP\n")}
    }

    "publish a message" in {
      val message = ByteString("Message")
      Publish("TestTopic", message).encodedRequest should be
      {ByteString("PUB TestTopic\n") ++ ByteString(0,0,0,7) ++ message}
    }

    "publish a message with unicode data" in {
      val message = ByteString("こんにちは")
      Publish("TestTopic", message).encodedRequest should be
      {ByteString("PUB TestTopic\n") ++ ByteString(0,0,0,15) ++ message}
    }

    "publish multiple messages" in {
      val message1 = ByteString("abcd")
      val message2 = ByteString("efgh")
      MultiPublish("TestTopic", Seq(message1, message2)).encodedRequest should be
      {ByteString("MPUB TestTopic\n") ++ ByteString(0,0,0,20) ++ ByteString(0,0,0,2) ++
        ByteString(0,0,0,4) ++ message1 ++
        ByteString(0,0,0,4) ++ message2}
    }

    "unpack message" in {
      val hexString = "132cb60626e9fd7a00013035356335626531636534333330323769747265616c6c7974" +
        "696564746865726f6f6d746f676574686572"
      val message = ByteString(new BigInteger(hexString, 16).toByteArray)
      val id = ByteString("055c5be1ce433027")
      val unpackedMessage = MessageUtils.decodeMessage(message)
      unpackedMessage.attempts should be {1}
      unpackedMessage.timestamp should be {1381679323234827642L}
      unpackedMessage.id should be {id}
    }
  }
}
