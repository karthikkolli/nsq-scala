## NSQ-Scala

`nsq-scala` is the scala client library for [NSQ][nsq].

Currently stable release is 0.1.0**

### Installation

    $ sbt clean
    $ sbt update
    $ sbt compile
    $ sbt run

### Docs

####To publish to a topic

    val system = ActorSystem("nsqSystem")
    val address = new InetSocketAddress("127.0.0.1", 4150)
    val config = NSQConfig(reconnectStrategy = ReconnectStrategy.EXPONENTIAL_RECONNECT)
    val producer = system.actorOf(Props(classOf[NSQProducer], address, "hello", config), "producer-1")
    
    producer ! StartProducer
    
    Thread.sleep(1000)
    
    (producer ? PublishMessage(ByteString("Hello")))
      .mapTo[Future[ProducerResponse]]
      .flatMap(identity)
      .onComplete {
      case Success(e) => println(e)
      case Failure(t) => println(t.getMessage)
    }

####To read from a topic
    class TestActor extends Actor with ActorLogging{
      var counter = 0
      override def receive: Receive = {
        case m: NSQMessage =>
          if((counter  % 10000) == 0) {
            println(counter)
          }
          counter += 1
          m.finish
      }
    }
    val system = ActorSystem("nsqSystem")
    val testActor = system.actorOf(Props(classOf[TestActor]), "testactor-1")
    val address = new InetSocketAddress("127.0.0.1", 4150)
    var lookupDAddress = new InetSocketAddress("127.0.0.1", 4161)


####To read from lookupd setup config as
    val config = NSQConfig(lookupHttpAddresses= Seq(lookupDAddress))

####To read from nsqd http addresses setup config as 
    val config = NSQConfig(nsqdTCPAddresses= Seq(address))

####To start the consumer, send StartConsumer message
    val consumer = system.actorOf(Props(classOf[NSQConsumer], "hello", "hello1", NSQConfig(lookupHttpAddresses= Seq(lookupDAddress)), testActor), "consumer-1")
    consumer ! StartConsumer

####NSQConfig
#####The number of connections or clients can be configured from NSQConfig
    maxConnections : Int = 4
    maxConnectionsPerAddress : Int = 2

#####The reconnect strategy for producer can be configured via config. 
#####EXPONENTIAL strategy takes steps between min and max durations. NONE strategy has no reconnect strategy. Server has to be reconnected manually by using producer ! RestartProducer 
    reconnectStrategy: ReconnectStrategy.ReconnectStrategy = ReconnectStrategy.EXPONENTIAL
    minReconnectDuration : FiniteDuration = 1.second
    maxReconnectDuration : FiniteDuration = 60.seconds

[nsq]: https://github.com/bitly/nsq
